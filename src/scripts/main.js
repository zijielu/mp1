// Put your js code here
console.log("Hello World!");

var video_height = document.getElementsByClassName("intro-video")[0].clientHeight;
var about_height = document.getElementsByClassName("about-text-container")[0].offsetHeight;
var poster_height = document.getElementsByClassName("poster")[0].clientHeight
var product_height = document.getElementById("product").clientHeight
var review_height = document.getElementById("review").clientHeight

var about = document.getElementById("about-button");
var product = document.getElementById("product-button");
var review = document.getElementById("review-button") 
var team = document.getElementById("team-button");

var carousel_item_class_name = "carousel_photo_container";
var carousel_items = document.getElementsByClassName(carousel_item_class_name);
var totalItems = carousel_items.length;
var slide = 0;
var moving = true;

// Modal initialization
var richard = document.getElementById("richard")
var modal_richard = document.getElementById("richard_modal")
var span_richard = document.getElementById("richard-close")

var jared = document.getElementById("jared")
var modal_jared = document.getElementById("jared_modal")
var span_jared = document.getElementById("jared_close")

var dinesh = document.getElementById("dinesh")
var modal_dinesh = document.getElementById("dinesh_modal")
var span_dinesh = document.getElementById("dinesh_close")

var gilfoyle = document.getElementById("gilfoyle")
var modal_gilfoyle = document.getElementById("gilfoyle_modal")
var span_gilfoyle = document.getElementById("gilfoyle_close")

initCarousel();
window.onscroll = function() {scrollFunction();}

setModalEventListener()
setModalCloseListner()

function scrollFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    
    console.log("current at" + winScroll)
    console.log("video height: " + video_height)
    console.log("about height: " + about_height)

    if (winScroll > about_height + video_height + poster_height + product_height + review_height) {
        resetButton()
        team.style.backgroundColor = "black";
    } else if (winScroll > about_height + video_height + poster_height + product_height) {
        resetButton()
        review.style.backgroundColor = "black";
    } else if (winScroll > about_height + video_height + poster_height) {
        resetButton()
        product.style.backgroundColor = "black";
    } else if (winScroll > video_height) {
        resetButton()
        about.style.backgroundColor = "black";
    } else {
        resetButton()
        // document.getElementById("about-button").style.backgroundColor = "#302E2E";
    }

    if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.getElementById("header").style.padding = "5px 10px";
        var lis = document.getElementsByClassName("nav-li");
        Array.prototype.forEach.call(lis, function(li) {
            li.style.fontSize = "15px";
        });
        document.getElementById("logo-title").style.fontSize = "1.5em";
    } else {
        document.getElementById("header").style.padding = "20px 10px";
        var lis = document.getElementsByClassName("nav-li");
        Array.prototype.forEach.call(lis, function(li) {
            li.style.fontSize = "20px";
        });
        document.getElementById("logo-title").style.fontSize = "2em";
    }
}

function resetButton() {
    var lis = document.getElementsByClassName("nav-li");
        Array.prototype.forEach.call(lis, function(li) {
            li.style.backgroundColor = "#302E2E";
    });
    // button.style.backgroundColor = "#302E2E"
}

function setInitialClasses() {
    carousel_items[totalItems - 1].classList.add("prev");
    carousel_items[0].classList.add("active");
    carousel_items[1].classList.add("next");
}

function setEventListeners() {
    var next_button = document.getElementsByClassName("carousel_button-next")[0];
    var prev_button = document.getElementsByClassName("carousel_button-prev")[0];
    next_button.addEventListener('click', moveNext);
    prev_button.addEventListener('click', movePrev);
}

function moveNext() {
    console.log("moveNext clicked");
    if (!moving) {
        slide = (slide + 1) % totalItems;
    }
    moveCarouselTo(slide, 1);
}

function movePrev() {
    console.log("movePrev clicked")
    if (!moving) {
        slide = (slide + totalItems - 1) % totalItems;
    }
    moveCarouselTo(slide, 0);
}

function disableInteraction() {
    moving = true;
    setTimeout(function() {
        moving = false;
    }, 500);
}

function moveCarouselTo(slide, dir) {
    var oldPrev;
    var oldNext;
    if (!moving) {
        disableInteraction();
        if (dir == 1) {
            oldPrev = (slide + totalItems - 2) % totalItems;
            oldNext = slide;
        } else {
            oldPrev = slide;
            oldNext = (slide + 1) % totalItems;
        }
        var newPrev = (slide + totalItems - 1) % totalItems;
        var newNext = (slide + 1) % totalItems 

        carousel_items[newPrev].className = carousel_item_class_name + " prev";
        carousel_items[slide].className = carousel_item_class_name + " active";
        carousel_items[newNext].className = carousel_item_class_name + " next";
    }
}

function initCarousel() {
    setInitialClasses();
    setEventListeners();

    moving = false;
}

function setModalEventListener() {
    richard.addEventListener('click', function() {
        modal_richard.style.display = "block";
    });
    
    jared.addEventListener('click', function() {
        modal_jared.style.display = "block";
    });

    dinesh.addEventListener('click', function() {
        modal_dinesh.style.display = "block";
    });
    
    gilfoyle.addEventListener('click', function() {
        modal_gilfoyle.style.display = "block";
    });
}

function setModalCloseListner() {
    span_richard.addEventListener('click', function() {
        modal_richard.style.display = 'none';
    });

    span_jared.addEventListener('click', function() {
        modal_jared.style.display = 'none';
    });

    span_dinesh.addEventListener('click', function() {
        modal_dinesh.style.display = 'none';
    });

    span_gilfoyle.addEventListener('click', function() {
        modal_gilfoyle.style.display = 'none';
    });
}



